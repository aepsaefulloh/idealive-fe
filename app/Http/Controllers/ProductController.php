<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(Request $request){
        $perpage = 15;
        $page = ($request->page != '') ? $request->page : 1;

        // Data Product
        $product = $this->http_get($this->url_api().'product?page='.$page.'&perpage='.$perpage);

        // dd($product['data']);
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
            $this->data['paginate_all'] = ceil($product['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['product'] = [];
            $this->data['paginate_all'] = 0;
        }
        $category = $this->http_get($this->url_api().'category');

        if ($category && $category['status'] == 200) {
            $this->data['category'] = $category['data'];
        } else {
            $this->data['category'] = [];
        }
        // dd($category['data']);
        
        return view('components.product.index', $this->data);

    }

    public function product_category(Request $request, $name){
        $perpage = 15;
        $page = ($request->page != '') ? $request->page : 1;

        $category = $this->http_get($this->url_api().'category/'.ucfirst(str_replace("-", " ", $name)));
        // Data Product
        $product = $this->http_get($this->url_api().'product/category/'.$category['data']['ID']);

        // dd($product['data']);
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
            $this->data['paginate_all'] = ceil($product['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['product'] = [];
            $this->data['paginate_all'] = 0;
        }
        $category = $this->http_get($this->url_api().'category');

        if ($category && $category['status'] == 200) {
            $this->data['category'] = $category['data'];
        } else {
            $this->data['category'] = [];
        }
        // dd($category['data']);
        
        return view('components.product.index', $this->data);

    }

    public function product_search(Request $request){
        $perpage = 15;
        $page = ($request->page != '') ? $request->page : 1;

        // $category = $this->http_get($this->url_api().'category/'.ucfirst(str_replace("-", " ", $name)));
        // Data Product
        $product = $this->http_get($this->url_api().'product/search/'.$request->q);

        // dd($product['data']);
        if ($product && $product['status'] == 200) {
            $this->data['product'] = $product['data'];
            $this->data['paginate_all'] = ceil($product['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['product'] = [];
            $this->data['paginate_all'] = 0;
        }
        $category = $this->http_get($this->url_api().'category');

        if ($category && $category['status'] == 200) {
            $this->data['category'] = $category['data'];
        } else {
            $this->data['category'] = [];
        }
        // dd($category['data']);
        
        return view('components.product.search', $this->data);

    }

    public function detail($id, $title){
        // Data Product Find
        $product = $this->http_get($this->url_api().'product/'.$id);
        if ($product && $product['status'] == 200 && isset($product['data']['IMAGE'])) {
            $this->data['product'] = $product['data'];
            // dd($this->data['product']);
        } else {
            return abort(404, 'Page not found');
        }

         // Data Product
         $productRelated = $this->http_get($this->url_api().'product?page=1&perpage=7');
            if ($productRelated && $productRelated['status'] == 200) {
                $this->data['productRelated'] = $productRelated['data'];
            } else {
                $this->data['productRelated'] = [];
            }
         return view('components.product.detail', $this->data);         
    }

//     public function cart(Request $req){
//         // Data Product
//         $data = \Session::get('cart');
//         dd($data);
//         return view('components.product.cart', ['data' => $data]);
//    }


//     public function addToCart(Request $req){
//         $data = [
//          'id' => $req->id,
//          'qty' => $req->qty,
//          'price' => $req->price,
//          'name' => $req->name,
//          'image' => $req->image,
//         ];
//         $req->session()->push('cart', $data);
//         return response()->json(['status' => true]);
//     } 
    
}