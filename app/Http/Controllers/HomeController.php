<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct() {
        session_start();
    }
    
    public function index(){
        // Data Content
        $content = $this->http_get($this->url_api().'content?category=1&perpage=6');
        if ($content && $content['status'] == 200) {
            $this->data['content'] = $content['data'];
        } else {
            $this->data['content'] = [];
        }

          // Data Project
          $project = $this->http_get($this->url_api().'project?perpage=4');
          if ($project && $project['status'] == 200) {
              $this->data['project'] = $project['data'];
          } else {
              $this->data['project'] = [];
          }

        // Data Client
        $client = $this->http_get($this->url_api().'content?category=2');
        if ($client && $client['status'] == 200) {
            $this->data['client'] = $client['data'];
        } else {
            $this->data['client'] = [];
        }
        
        return view('home', $this->data);
    }

 

  
}