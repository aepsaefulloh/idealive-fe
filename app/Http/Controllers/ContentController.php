<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;

class ContentController extends Controller
{
    public function __construct() {
        session_start();
    }

    public function index(Request $request){
        $perpage = 10;
        $page = ($request->page != '') ? $request->page : 1;

        // Data content
        $content = $this->http_get($this->url_api().'content?category=1&page='.$page.'&perpage='.$perpage);
        // dd($content['data']);

        if ($content && $content['status'] == 200 && count($content['data']) > 0) {
            $this->data['content'] = $content['data'];
            
            $this->data['paginate_all'] = ceil($content['links']['parameters']['count_data']/$perpage);
        } else {
            $this->data['content'] = [];

            $this->data['paginate_all'] = 0;
        }
        $this->data['pop_roll_current'] = $content['data'];

        return view('components.article.index', $this->data);
    }

    public function detail($id, $title){
        // Data content Find
        $content = $this->http_get($this->url_api().'content/'.$id);
        // echo($content);
        if ($content && $content['status'] == 200 && isset($content['data']['IMAGE'])) {
            $this->data['content'] = $content['data'];
            return view('components.article.detail', $this->data);
        } else {
            return abort(404, 'Page not found');
        }
        
    }
    

}