<nav class="navbar navbar-expand-lg bg-transparent">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{asset('assets')}}/images/idealive-logo-black.png" class="img-fluid" width="100" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#navbarOffcanvasLg"
            aria-controls="offcanvasNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="offcanvas offcanvas-end" tabindex="-1" id="navbarOffcanvasLg"
            aria-labelledby="offcanvasNavbarLabel">
            <div class="offcanvas-header">
                <h5 class="offcanvas-title" id="offcanvasNavbarLabel">
                    <img src="{{asset('assets')}}/images/idealive-logo-black.png" class="img-fluid" width="100" alt="">

                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav justify-content-center flex-grow-1 pe-3">
                    <li class="nav-item">
                        <a class="nav-link text-uppercase active" aria-current="page" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-uppercase" href="{{url('project')}}">Portfolio</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-uppercase" href="{{url('contact')}}">Contact</a>
                    </li>
                </ul>
                <div class="navbar-text">
                    <div>CALL US:
                        <a href="tel:{{ (isset($settings['Phone']['CVALUE'])) ? $settings['Phone']['CVALUE'] : null }}">
                            <span>&nbsp;{{ (isset($settings['Phone']['CVALUE'])) ? $settings['Phone']['CVALUE'] : null }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>