<footer class="section-footer py-50">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-12">
                <div class="footer-logo">
                    <img src="{{asset('assets')}}/images/idealive-logo-white.png" class="img-fluid"
                        alt="idealive-logo-footer">
                </div>
                <div class="footer-title text-center mb-4">
                    <a
                        href="tel:{{ (isset($settings['Phone']['CVALUE'])) ? $settings['Phone']['CVALUE'] : null }}">{{ (isset(Helper::config_phone()['CVALUE'])) ? Helper::config_phone()['CVALUE'] : null }}</a>
                </div>
            </div>
            <div class="col-md-3 col-6">
                <div class="footer-title">Quick Links</div>
                <ul>
                    <!-- <li><a href="">Home</a></li> -->
                    <!-- <li><a href="">Gallery</a></li> -->
                    <li><a href="{{url('/project')}}">Portfolio</a></li>
                    <li><a href="{{url('/contact')}}">Contact</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-6">
                <div class="footer-title">Career</div>
                <ul>
                    <!-- <li><a href="{{ (isset($settings['Facebook']['CVALUE'])) ? $settings['Facebook']['CVALUE'] : null }}"
                            class="text-uppercase">facebook</a></li>
                    <li><a href="{{ (isset($settings['Twitter']['CVALUE'])) ? $settings['Twitter']['CVALUE'] : null }}"
                            class="text-uppercase">twitter</a></li>
                    <li><a href="{{ (isset($settings['Instagram']['CVALUE'])) ? $settings['Instagram']['CVALUE'] : null }}"
                            class="text-uppercase">instagram</a></li>
                    <li><a href="{{ (isset($settings['Linkedin']['CVALUE'])) ? $settings['Linkedin']['CVALUE'] : null }}"
                            class="text-uppercase">linkedin</a></li> -->
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">Intership</a></li>

                </ul>
            </div>
            <div class="col-md-3 col-12">
                <div class="footer-title mt-5 mt-md-0">Sign up to our newsletter</div>
                <form action="">
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="Email" aria-label="Email">
                        <button class="btn" type="submit"><img
                                src="{{asset('assets')}}/images/vector/arrow-up-right-white.svg" width="30"
                                class="img-fluid" alt=""></button>
                        <p class="text-white">This site is protected by reCAPTHCHA and the <a
                                href="https://policies.google.com/privacy?hl=en-US" target="_blank"
                                class="text-decoration-underline">Google Privacy
                                Policy</a> and
                            <a href="https://policies.google.com/terms?hl=en-US" class="text-decoration-underline">Terms
                                of Service
                                apply</a>.
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="div-line"></div>
            <div class="row">
                <div class="col-md-9">
                    <div class="copyright-text">
                        Copyright © 2022 Idealive.
                        All Rights Reserved.
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-flex justify-content-between">
                        <div class="copyright-link">
                            <a href="">Terms of Use</a>
                        </div>
                        <div class="copyright-link">
                            <a href="">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>