@extends('components.master')
{{-- Meta Tag HTML --}}
@section('link-cannonical', url()->current())
@section('meta-desc', strip_tags($content['SUMMARY']))
@section('meta-fb-title', $content['TITLE'])
@section('meta-fb-desc', strip_tags($content['SUMMARY']))
@section('meta-fb-image', $content['IMAGE'])
@section('meta-fb-url', url()->current())
{{-- END META TAG --}}
@section('content')

@endsection

@push('scripts')

@endpush