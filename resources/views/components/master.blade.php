<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="@yield('link-cannonical')" />
    <title>Idealive - We Brings Your Brands Alive</title>
    <meta name="description" content="@yield('meta-desc')">
    <meta name="keywords" content="@yield('meta-keywords')">
    <meta name="author" content="Floothink">
    <link rel="shortcut icon" href="{{asset('assets')}}/images/favicon.png?<?php echo rand()?>" type="image/x-icon">
    <link rel="icon" href="{{asset('assets')}}/images/favicon.png?<?php echo rand()?>" type="image/x-icon">
    <!-- Facebook & Twitter Meta -->
    <meta property="og:title" content="@yield('meta-fb-title')">
    <meta property="og:type" content="website" />
    <meta property="og:description" content="@yield('meta-fb-desc')">
    <meta property="og:image" content="@yield('meta-fb-image')">
    <meta property="og:url" content="@yield('meta-fb-url')">
    <meta name="twitter:card" content="summary_large_image">
    <!-- Vendor -->
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/swiperjs/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{asset('assets')}}/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <!-- Custom only -->
    <link rel="stylesheet" href="{{asset('assets')}}/css/style.css?v=<?php echo rand() ?>">
    <link rel="stylesheet" href="{{asset('assets')}}/css/responsive.css?v=<?php echo rand() ?>">
    <link rel="stylesheet" href="{{asset('assets')}}/css/animation.css?v=<?php echo rand() ?>">
    <link rel="stylesheet" href="{{asset('assets')}}/css/font.css">



</head>

<body>
    @include('components.include.navbar')
    @yield('content')
    @include('components.include.footer')
    <script src="{{asset('assets')}}/vendor/jquery/jquery.min.js"></script>
    <script src="{{asset('assets')}}/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="{{asset('assets')}}/vendor/swiperjs/swiper-bundle.min.js"></script>
    <script src="https://unpkg.com/scrollreveal@4.0.0/dist/scrollreveal.min.js"></script>
    <script src="{{asset('assets')}}/js/custom.js?v=<?php echo rand() ?>"></script>
    @stack('scripts')

</body>

</html>