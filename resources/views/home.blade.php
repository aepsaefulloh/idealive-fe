@extends('components.master')

@section('content')

<section class="section-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="hero-image">
                    <img src="{{asset('assets')}}/images/idealive-circle.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-md-9 align-items-center">
                <div class="hero-text">
                    <h1>IDEALIVE CREATIVE</h1>
                    <h1><span class="typed-text"></span><span class="cursor">&nbsp;</span></h1>
                </div>
                <div class="hero-navigation">
                    <a href="#"><img src="{{asset('assets')}}/images/vector/arrow-up-right.svg" class="img-fluid"
                            alt="">&nbsp;About Us</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-wording">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-md-8 ms-auto">
                <p>Born in Jakarta circa 2017 driven by profound interest in building connections between brands &
                    audiences. We are your partner to achieve your business ambitions by delivering meaningful
                    engagement to you consumers.</p>
            </div>
        </div>
    </div>
    <div class="wording-image">
        <img src="{{asset('assets')}}/images/about-new.jpg" class="w-100" alt="">
    </div>
</section>

<section class="section-services">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="heading">
                    <h2>Our Services</h2>
                    <p>Kami telah bekerja dengan beragam klien di seluruh dunia untuk menerapkan dasar-dasar desain
                        keanggunan, Simplicity</p>
                </div>
                <div class="accordion accordion-flush" id="accordionFlushExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingOne">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne" aria-expanded="false"
                                aria-controls="flush-collapseOne">
                                Brand Strategy & Story Building
                            </button>
                        </h2>
                        <div id="flush-collapseOne" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Define your brand identity and & position to reach consumers
                                hearts.</div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                aria-controls="flush-collapseTwo">
                                360° Integrated Marketing Experience
                            </button>
                        </h2>
                        <div id="flush-collapseTwo" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Complete activation on you consumers multiple touch point.</div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseThree" aria-expanded="false"
                                aria-controls="flush-collapseThree">
                                Digital Media Planning & Buying
                            </button>
                        </h2>
                        <div id="flush-collapseThree" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Reach your objective with well-planned strategy and continuous
                                iteration.</div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseFour" aria-expanded="false"
                                aria-controls="flush-collapseFour">
                                Community Based Activation
                            </button>
                        </h2>
                        <div id="flush-collapseFour" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Exclusive collaboration to engage consumers at more intimate
                                level.</div>
                        </div>
                    </div>

                    <div class="accordion-item">
                        <h2 class="accordion-header" id="flush-headingFive">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseFive" aria-expanded="false"
                                aria-controls="flush-collapseFive">
                                On-ground Activation
                            </button>
                        </h2>
                        <div id="flush-collapseFive" class="accordion-collapse collapse"
                            aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                            <div class="accordion-body">Deliver experiential journey to your consumers directly.</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="section-client">

    <div class="heading text-center">
        <h3>We Dont just excecute ideas,
            we bring it alive.</h3>
    </div>
    <div class="client-slide">
        <div dir="rtl" class="swiper ClientCarousel">
            <div class="swiper-wrapper">
                @forelse($client as $item)
                <div class="swiper-slide">
                    <div class="client-swiper">
                        <img src="{{$item['IMAGE']}}" class="img-fluid" alt="{{$item['TITLE']}}" />
                    </div>
                </div>
                @empty
                @endforelse
            </div>

        </div>
    </div>


</section>
@endsection

@push('scripts')
<script>
var swiper = new Swiper(".ClientCarousel", {
    parallax: true,
    speed: 600,
    slidesPerView: 2,
    grid: {
        rows: 1,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    loop: true,
    freeMode: true,
    spaceBetween: 10,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        768: {
            slidesPerView: 6,
            grid: {
                rows: 2,
            },
        },
    },
});

// Typing
const typedTextSpan = document.querySelector(".typed-text");
const cursorSpan = document.querySelector(".cursor");

const textArray = ["Engine", "Laboratory", "Agency"];
const typingDelay = 200;
const erasingDelay = 100;
const newTextDelay = 2000; // Delay between current and next text
let textArrayIndex = 0;
let charIndex = 0;

function type() {
    if (charIndex < textArray[textArrayIndex].length) {
        if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
        typedTextSpan.textContent += textArray[textArrayIndex].charAt(charIndex);
        charIndex++;
        setTimeout(type, typingDelay);
    } else {
        cursorSpan.classList.remove("typing");
        setTimeout(erase, newTextDelay);
    }
}

function erase() {
    if (charIndex > 0) {
        if (!cursorSpan.classList.contains("typing")) cursorSpan.classList.add("typing");
        typedTextSpan.textContent = textArray[textArrayIndex].substring(0, charIndex - 1);
        charIndex--;
        setTimeout(erase, erasingDelay);
    } else {
        cursorSpan.classList.remove("typing");
        textArrayIndex++;
        if (textArrayIndex >= textArray.length) textArrayIndex = 0;
        setTimeout(type, typingDelay + 1100);
    }
}

document.addEventListener("DOMContentLoaded", function() { // On DOM Load initiate the effect
    if (textArray.length) setTimeout(type, newTextDelay + 250);
});
</script>

@endpush