@extends('components.master')

@section('content')
<section class="section-contact-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="heading w-100">
                    <h2 class="title text-outline-white text-uppercase">Contact</h2>
                </div>
                <div class="row mt-3">
                    <div class="col-md-8">
                        <div class="nav-text">
                            <h6>
                                <a
                                    href="#">{{ (isset($settings['Address']['CVALUE'])) ? $settings['Address']['CVALUE'] : null }}</a>
                            </h6>
                            <h3 class="mt-5">
                                <a
                                    href="tel:{{ (isset($settings['Alt Phone']['CVALUE'])) ? $settings['Alt Phone']['CVALUE'] : null }}">{{ (isset($settings['Alt Phone']['CVALUE'])) ? $settings['Alt Phone']['CVALUE'] : null }}</a>
                            </h3>
                            <h3 class="py-1">
                                <a
                                    href="tel:{{ (isset($settings['Phone']['CVALUE'])) ? $settings['Phone']['CVALUE'] : null }}">{{ (isset($settings['Phone']['CVALUE'])) ? $settings['Phone']['CVALUE'] : null }}</a>
                            </h3>
                            <h3>
                                <a
                                    href="mailto:{{ (isset($settings['Email']['CVALUE'])) ? $settings['Email']['CVALUE'] : null }}">{{ (isset($settings['Email']['CVALUE'])) ? $settings['Email']['CVALUE'] : null }}</a>
                            </h3>
                        </div>
                        <!-- <div class="social-media-text">
                            <ul>
                                <li><a
                                        href="{{ (isset($settings['Facebook']['CVALUE'])) ? $settings['Facebook']['CVALUE'] : null }}">facebook</a>
                                </li>
                                <li><a
                                        href="{{ (isset($settings['Twitter']['CVALUE'])) ? $settings['Twitter']['CVALUE'] : null }}">twitter</a>
                                </li>
                                <li><a
                                        href="{{ (isset($settings['Instagram']['CVALUE'])) ? $settings['Instagram']['CVALUE'] : null }}">instagram</a>
                                </li>
                                <li><a
                                        href="{{ (isset($settings['Linkedin']['CVALUE'])) ? $settings['Linkedin']['CVALUE'] : null }}">linkedin</a>
                                </li>
                            </ul>
                        </div> -->

                    </div>

                </div>
            </div>
            <div class="col-md-6 align-self-center">
                <div class="form-contact">
                    <form method="post" id="contact-form">
                        <div class="mb-4">
                            <input type="text" class="form-control" placeholder="Name" name="FULLNAME" id="fullname" />
                        </div>
                        <div class="mb-4">
                            <input type="text" class="form-control" placeholder="Email" name="EMAIL" id="email" />
                        </div>
                        <div class="mb-4">
                            <textarea class="form-control" rows="3" placeholder="Message" name="MESSAGE" id="message"></textarea>
                        </div>

                        <button type="submit" class="btn btn-white" id="btn-contact">Submit <img
                                src="{{asset('assets')}}/images/vector/arrow-up-right.svg" class="img-fluid ms-5" alt=""
                                width="15"></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="line"> </div>
                <div class="grid-contact-information">
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            General & Business
                        </div>
                        <!-- <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div> -->

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Jobs
                        </div>
                        <!-- <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div> -->

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Intership
                        </div>
                        <!-- <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div> -->

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                    <div class="grid-item">
                        <div class="contact-widget-title">
                            Workshop And Talks
                        </div>
                        <!-- <div class="caption">
                            For new business inquiries, send your project brief to the email below and we will get back
                            to
                            you soon. And we’re not shying away from collaborations or community project, so send us
                            your
                            proposal also to this email
                        </div> -->

                        <div class="link">
                            <a href="">info@Idealive.asia</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection

@push('scripts')
<script src="{{asset('assets')}}/vendor/sweetalert2/sweetalert.js"></script>

<script>
$("#contact-form").submit(function(e) {
    event.preventDefault();
    $("#contact-form").attr("disabled", "disabled");
    var fullname = $('#fullname').val();
    var email = $('#email').val();
    var message = $('#message').val();
    if (fullname != "" && email != "" && message != "") {
        $.ajax({
            url: '{{route("contact.store")}}',
            type: 'POST',
            data: {
                fullname: fullname,
                email: email,
                message: message,
                _token: "{{ csrf_token() }}"
            },
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.status == 200) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Message successfully sent',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function() {
                        location.reload();
                        // button disabled
                        $(".btn-submit-1").attr("disabled", "disabled");
                    })
                    $("#btn-contact")[0].reset();
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'something went wrong. Please try again',
                    })
                }
            }
        });
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'something went wrong. Please try again',
        })
    }
});
</script>

@endpush