// Scroll Reveal
ScrollReveal().reveal('.section-wording p', {
    delay: 100
});
ScrollReveal().reveal('.section-wording .wording-image img', {
    delay: 500
});
ScrollReveal().reveal('.section-services .accordion .accordion-item', {
    delay: 1000,
    easing: 'ease-in'
});

ScrollReveal().reveal('.section-client .client-slide', {
    delay: 1000
});

