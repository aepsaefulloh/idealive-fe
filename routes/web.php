<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/article', [ArticleController::class, 'index'])->name('article');

Route::get('/article-detail/{id}/{title}', [ArticleController::class, 'detail'])->name('article.detail');

Route::get('/about', [AboutController::class, 'index'])->name('about');

Route::get('/project/category/{name}', [ProjectController::class, 'project_category'])->name('project');

Route::get('/project', [ProjectController::class, 'index'])->name('project');

Route::get('/project-detail/{id}/{title}', [ProjectController::class, 'detail'])->name('project.detail');

Route::get('/contact', [ContactController::class, 'index'])->name('contact');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::get('/promo', [PromoController::class, 'index'])->name('promo');

Route::post('/add-cart', [PaymentController::class, 'add_cart'])->name('add_cart');
Route::get('/remove-cart', [PaymentController::class, 'remove_cart'])->name('remove_cart');
Route::get('/cart', [PaymentController::class, 'cart'])->name('cart');

Route::get('/order-success', [PaymentController::class, 'orderSuccess'])->name('order-success');